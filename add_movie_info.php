<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link href="https://fonts.googleapis.com/css?family=Lato|Lobster" rel="stylesheet">
	<link rel="stylesheet" href="css/app.css">
</head>
<body>
	<div class="page">
		<div class="row">
			<div class="header">
				<h2>Add Movie Information</h2>
			</div>	
		</div>

		<div class="row">
			<div class="sidebar">
				<ul class="nav">
					<p class="navtitle">Home</p>
					<li>
						<a href="index.php">Home</a>
					</li>
				</ul>
				<ul class="nav">
					<p class="navtitle">Add new content</p>
					<li>
						<a href="add_actor_director.php">Add Actor/Director</a>
					</li>
					<li>
						<a href="add_movie_info.php">Add Movie Information</a>
					</li>
					<li>
						<a href="add_movie_actor_relation.php">Add Movie/Actor Relation</a>
					</li>
					<li>
						<a href="add_movie_director_relation.php">Add Movie/Director Relation</a>
					</li>
				</ul>
				<ul class="nav">
					<p class="navtitle">Search</p>
					<li>
						<a href="search.php">Search Actor/Movie</a>
					</li>
				</ul>
			</div>

			<div class="content">
				<form method="GET" action="#">
					<div class="form-group">
						<label for="title">
							Title
						</label>
						<input type="text" placeholder="Text input" name="title">
					</div>
					<div class="form-group">
						<label for="company">
							Company
						</label>
						<input type="text" placeholder="Text input" name="company">
					</div>
					<div class="form-group">
						<label for="year">
							Year
						</label>
						<input type="text" placeholder="Text input" name="year">
					</div>
					<div class="form-group">
						<label for="rating">
							MPAA Rating
						</label>
						<select name="rating">
							<option value="G">G</option>
							<option value="NC-17">NC-17</option>
							<option value="PG">PG</option>
							<option value="PG-13">PG-13</option>
							<option value="R">R</option>
							<option value="surrendere">surrendere</option>
						</select>
					</div>
					<div class="form-group">
						<label for="genre">
							Genre
						</label>
						<input type="checkbox" name="genre[]" value="Action"> Action
						<input type="checkbox" name="genre[]" value="Adult"> Adult
						<input type="checkbox" name="genre[]" value="Adventure"> Adventure
						<input type="checkbox" name="genre[]" value="Animation"> Animation
						<input type="checkbox" name="genre[]" value="Comedy"> Comedy
						<input type="checkbox" name="genre[]" value="Crime"> Crime
						<input type="checkbox" name="genre[]" value="Documentary"> Documentary
						<input type="checkbox" name="genre[]" value="Drama"> Drama
						<input type="checkbox" name="genre[]" value="Family"> Family
						<input type="checkbox" name="genre[]" value="Fantasy"> Fantasy
						<input type="checkbox" name="genre[]" value="Horror"> Horror
						<input type="checkbox" name="genre[]" value="Musical"> Musical
						<input type="checkbox" name="genre[]" value="Mystery"> Mystery
						<input type="checkbox" name="genre[]" value="Romance"> Romance
						<input type="checkbox" name="genre[]" value="Sci-Fi"> Sci-Fi
						<input type="checkbox" name="genre[]" value="Short"> Short
						<input type="checkbox" name="genre[]" value="Thriller"> Thriller
						<input type="checkbox" name="genre[]" value="War"> War
						<input type="checkbox" name="genre[]" value="Western"> Western
					</div>
					<button type="submit">Add</button>
				</form>
			</div>		
		</div>
	</div>

	<?php
		// Connect to Database //
		$db = new mysqli('localhost', 'cs143', '', 'CS143');

		// Connect to db //
		if ($db->connect_errno > 0) {
			die('Unable to connect to database ['.$db->connect_error.']');
		}

		// Process input //
		if ($_GET) {

			$title    = $_GET['title'];
			$company = $_GET['company'];
			$year    = $_GET['year'];
			$rating    = $_GET['rating'];
			$genres   = $_GET['genre'];

			// Validate inputs
			if (!$title)
				echo "Need title";
			else if (!$company)
				echo "Need company name";
			else if ($year < 1900 || $year > 2016)
				echo "Not a valid year";
			
			// Inputs are valid
			else {
				// retrieve new movie id
				$max_mid = $db->query("
					SELECT *
					 FROM MaxMovieID
				")->fetch_assoc()['id'];

				$new_max_mid = $max_mid + 1;

				// write the query
				$query = "INSERT INTO Movie (id, title, year, rating, company) VALUES($new_max_mid, '$title', '$year', '$rating', '$company')";

				// make the query bitch
				if ($db->query($query)) { // insert successful
					// update MaxMovieID for succesful insertion
					$db->query("
						UPDATE MaxMovieID
						SET id=$new_max_mid
						WHERE id=$max_mid
					");
					echo "Added successfully";

					for ($i = 0; $i < count($genres); $i++) {
						$db->query("
							INSERT INTO MovieGenre (mid, genre)
							VALUES ($new_max_mid, '$genres[$i]')
						");
					}

				} else { // error lol
					echo "im ded rip";
				}
			}
		}
	?>
</body>
</html>
