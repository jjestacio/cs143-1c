<?php 
	// Connect to Database //
	$db = new mysqli('localhost', 'cs143', '', 'CS143');

	// Connect to db //
	if ($db->connect_errno > 0) {
		die('Unable to connect to database ['.$db->connect_error.']');
	}

	// Process input
	if ($_GET) {

		// Split query into separate words
		$query_parts = explode(' ', $_GET['query']);

		$actor_query = "
			SELECT id, last, first, dob
			FROM Actor
			WHERE (
				first LIKE '%$query_parts[0]%'
				OR last LIKE '%$query_parts[0]%'
			)
		";

		$movie_query = "
			SELECT id, title, year
			FROM Movie
			WHERE title LIKE '%$query_parts[0]%'
		";

		// Add additional restrictions to query
		for ($i = 1; $i < count($query_parts); $i++) {
			$actor_query .= " AND (
				first LIKE '%$query_parts[$i]%'
				OR last LIKE '%$query_parts[$i]%'
			)";

			$movie_query .= " AND title LIKE '%$query_parts[$i]%'";
		}

		$actors = $db->query($actor_query);
		$movies = $db->query($movie_query);

		$actors_html = "<table border=\"1\">";
		$movies_html = "<table border=\"1\">";

		// Add table headers
		$actors_html .= "
			<tr>
				<td>Name</td>
				<td>Date of Birth</td>
			</tr>
		";

		$movies_html .= "
			<tr>
				<td>Title</td>
				<td>Year</td>
			</tr>
		";

		// Create actors html
		while ($row = $actors->fetch_array()) {
			$id = $row['id'];
			$first = $row['first'];
			$last = $row['last'];
			$dob = $row['dob'];

			$actors_html .= "
				<tr>
					<td><a href=\"actor_info.php?id={$id}\">{$first} {$last}</td>
					<td><a href=\"actor_info.php?id={$id}\">{$dob}</td>
				</tr>
			";
		}

		// Create movies html
		while ($row = $movies->fetch_array()) {
			$id = $row['id'];
			$title = $row['title'];
			$year = $row['year'];
		
			$movies_html .= "
				<tr>
					<td><a href=\"movie_info.php?id={$id}\">{$title}</td>
					<td><a href=\"movie_info.php?id={$id}\">{$year}</td>
				</tr>
			";
		}

		$actors_html .= "</table>";
		$movies_html .= "</table>";
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link href="https://fonts.googleapis.com/css?family=Lato|Lobster" rel="stylesheet">
	<link rel="stylesheet" href="css/app.css">
</head>
<body>
	<div class="page">
		<div class="row">
			<div class="header">
				<h2>Search</h2>	
			</div>	
		</div>
		<div class="row">
			<div class="sidebar">
				<ul class="nav">
					<p class="navtitle">Home</p>
					<li>
						<a href="index.php">Home</a>
					</li>
				</ul>
				<ul class="nav">
					<p class="navtitle">Add new content</p>
					<li>
						<a href="add_actor_director.php">Add Actor/Director</a>
					</li>
					<li>
						<a href="add_movie_info.php">Add Movie Information</a>
					</li>
					<li>
						<a href="add_movie_actor_relation.php">Add Movie/Actor Relation</a>
					</li>
					<li>
						<a href="add_movie_director_relation.php">Add Movie/Director Relation</a>
					</li>
				</ul>
				<ul class="nav">
					<p class="navtitle">Search</p>
					<li>
						<a href="search.php">Search Actor/Movie</a>
					</li>
				</ul>
			</div>
			<div class="content">
				<div class="form-group">
					<form action="#" method="GET">
						<input type="text" placeholder="Text input" name="query">
						<button type="submit">Search</button>
					</form>
				</div>
				<div class="actors">
					<h2>Actors</h2>
					<?= $actors_html ?>
				</div>
				<div class="movies">
					<h2>Movies</h2>
					<?= $movies_html ?>
				</div>
			</div>		
		</div>
	</div>
</body>
</html>