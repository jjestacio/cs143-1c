<?php 
	// Connect to Database //
	$db = new mysqli('localhost', 'cs143', '', 'CS143');

	// Connect to db //
	if ($db->connect_errno > 0) {
		die('Unable to connect to database ['.$db->connect_error.']');
	}

	if ($_GET['id']) {

		$movie_id = $_GET['id'];

		// Get movie information
		$movie = $db->query("
			SELECT *
			FROM Movie
			WHERE id={$movie_id}
		");

		// Actors in movies
		$actors = $db->query("
			SELECT A.id, A.first, A.last, MA.role
			FROM MovieActor MA, Actor A
			WHERE MA.mid={$movie_id} AND MA.aid=A.id
		");

		// Director .. Are there multiple ??
		$director = $db->query("
			SELECT D.id, D.first, D.last
			FROM MovieDirector MD, Director D
			WHERE MD.mid={$movie_id} AND MD.did=D.id
		");

		$director = $director 
			? "{$director->fetch_array()['first']} {$director->fetch_array()['last']}"
			: "";

		// Genre
		$genres = $db->query("
			SELECT genre
			FROM MovieGenre MG
			WHERE MG.mid={$movie_id}
		");

		// Reviews

		$reviews = $db->query("
			SELECT *
			FROM Review
			WHERE mid={$movie_id}
		");

		$movie_html = "<table border=\"1\">";
		$actors_html = "<table border=\"1\">";
		$add_review_html = "<a href=\"add_review.php?mid={$movie_id}\">Add a Review!</a>";
		$reviews_html = "";

		// Add table headers
		$movie_html .= "
			<tr>
				<td><b>Title</b></td>
				<td><b>Year</b></td>
				<td><b>MPAA Rating</b></td>
				<td><b>Producer</b></td>
				<td><b>Director</b></td>
				<td><b>Genre</b></td>
			</tr>
		";

		$actors_html .= "
			<tr>
				<td><b>Name</b></td>
				<td><b>Role</b></td>
			</tr>
		";

		// Create movie information html
		while ($row = $movie->fetch_array()) {
			$title = $row['title'];
			$year = $row['year'];
			$rating = $row['rating'];
			$company = $row['company'] ? $row['company'] : 'N/A';

			$movie_html .= "
				<tr>
					<td>{$title}</td>
					<td>{$year}</td>
					<td>{$rating}</td>
					<td>{$company}</td>
					<td>{$director}</td>
					<td>
			";

			// Add genres
			while ($row = $genres->fetch_array())
				$movie_html .= "{$row['genre']}<br>";

			$movie_html .= "</td></tr>";
		}

		// Create list of actors in the movie
		while ($row = $actors->fetch_array()) {
			$id = $row['id'];
			$first = $row['first'];
			$last = $row['last'];
			$role = $row['role'];

			$actors_html .= "
				<tr>
					<td><a href=\"actor_info.php?id={$id}\">{$first} {$last}</td>
					<td>{$role}</td>
				</tr>
			";
		}

		// List reviews for the movie
		while ($row = $reviews->fetch_array()) {
			$name = $row['name'];
			$time = $row['time'];
			$rating = $row['rating'];
			$comment = $row['comment'];

			$reviews_html .= "
				<div class=\"review\">
					<span><b>Author: </b>{$name}</span>
					<span><b>Rating: </b>{$rating}</span>
					<span><b>Time: </b>{$time}</span>
					<div class=\"comment\">
						{$comment}
					</div>
				</div>
			";
		}

		$movie_html .= "</table>";
		$actors_html .= "</table>";
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link href="https://fonts.googleapis.com/css?family=Lato|Lobster" rel="stylesheet">
	<link rel="stylesheet" href="css/app.css">
</head>
<body>
	<div class="page">
		<div class="row">
			<div class="header">
				<h2>Movie Information</h2>	
			</div>	
		</div>
		<div class="row">
			<div class="sidebar">
				<ul class="nav">
					<p class="navtitle">Home</p>
					<li>
						<a href="index.php">Home</a>
					</li>
				</ul>
				<ul class="nav">
					<p class="navtitle">Add new content</p>
					<li>
						<a href="add_actor_director.php">Add Actor/Director</a>
					</li>
					<li>
						<a href="add_movie_info.php">Add Movie Information</a>
					</li>
					<li>
						<a href="add_movie_actor_relation.php">Add Movie/Actor Relation</a>
					</li>
					<li>
						<a href="add_movie_director_relation.php">Add Movie/Director Relation</a>
					</li>
				</ul>
				<ul class="nav">
					<p class="navtitle">Search</p>
					<li>
						<a href="search.php">Search Actor/Movie</a>
					</li>
				</ul>
			</div>
			</div>
			<div class="content">
				<div class="actor_info">
					<h2>Movie Information</h2>
					<?= $movie_html ?>
				</div>
				<div class="movies_info">
					<h2>Actors</h2>
					<?= $actors_html ?>
				</div>
				<div class="add_review">
					<h2>Add a Review</h2>
					<?= $add_review_html ?>
				</div>
				<div class="reviews">
					<h2>Reviews</h2>
					<?= $reviews_html ?>
				</div>
			</div>		
		</div>
	</div>
</body>
</html>