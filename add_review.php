<?php
	// Connect to Database //
	$db = new mysqli('localhost', 'cs143', '', 'CS143');

	// Connect to db //
	if ($db->connect_errno > 0) {
		die('Unable to connect to database ['.$db->connect_error.']');
	}

	// query the db to get movie title from mid param
	if ($_GET) {
		$mid = $_GET['mid'];

		$movie = $db->query("
			SELECT *
			FROM Movie
			WHERE id={$mid}
		")->fetch_array();

		// if (!$movie) { // error in query
		// 	echo "Error: " . $query . "<br/>" . $db->error;
		// }

		
		$movie_title = $movie['title'];
		$movie_year = $movie['year'];

		$movie_option_html = "<option value=\"$mid\">{$movie_title} ({$movie_year})</option>";
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link href="https://fonts.googleapis.com/css?family=Lato|Lobster" rel="stylesheet">
	<link rel="stylesheet" href="css/app.css">
</head>
<body>
	<div class="page">
		<div class="row">
			<div class="header">
				<h2>Add A Review</h2>
			</div>	
		</div>

		<div class="row">
			<div class="sidebar">
				<ul class="nav">
					<p class="navtitle">Home</p>
					<li>
						<a href="index.php">Home</a>
					</li>
				</ul>
				<ul class="nav">
					<p class="navtitle">Add new content</p>
					<li>
						<a href="add_actor_director.php">Add Actor/Director</a>
					</li>
					<li>
						<a href="add_movie_info.php">Add Movie Information</a>
					</li>
					<li>
						<a href="add_movie_actor_relation.php">Add Movie/Actor Relation</a>
					</li>
					<li>
						<a href="add_movie_director_relation.php">Add Movie/Directoinr Relation</a>
					</li>
				</ul>
				<ul class="nav">
					<p class="navtitle">Search</p>
					<li>
						<a href="search.php">Search Actor/Movie</a>
					</li>
				</ul>
			</div>

			<div class="content">
				<form method="GET" action="#">
					<div class="form-group">
						<label for="name">MovieID</label>
						<select name="mid" id="mid">
							<?= $movie_option_html ?>
						</select>
					</div>
					<div class="form-group">
						<label for="name">Name</label>
						<input type="text" placeholder="Text input" name="name">
					</div>
					<div class="form-group">
						<label for="rating">Rating</label>
						<select name="rating">
							<option value=1>1</option>
							<option value=2>2</option>
							<option value=3>3</option>
							<option value=4>4</option>
							<option value=5>5</option>
						</select>
					</div>

					<div class="form-group">
						<label for="review_text">Review</label>
						<textarea name="review_text" rows=16 cols=64></textarea>
					</div>
					<button type="submit">Submit</button>
				</form>
			</div>		
		</div>
	</div>
	<?php 
		if ($_GET['rating']) {
			$mid         = $_GET['mid'];
			$name        = $_GET['name'];
			$rating      = $_GET['rating'];
			$review_text = $_GET['review_text'];

			// if ($valid_review_tuple($mid, $name, $rating, $review_text)) {
			// 	die("invalid");
			// }

			echo "Here";

			// tuple is valid

			// write the query
			$query = "INSERT INTO Review VALUES('$name', now(), $mid, $rating, '$review_text')";

			// make the query
			if ($db->query($query)) {
				echo "Added successfully";
			} else { // error lol
				echo "im ded rip";
			}

			// function valid_review_tuple($mid, $name, $rating, $review_text) {
			// 	return true;
			// }
		}
	?>
</body>
</html>
