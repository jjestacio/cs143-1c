<?php 
	// Connect to Database //
	$db = new mysqli('localhost', 'cs143', '', 'CS143');

	// Connect to db //
	if ($db->connect_errno > 0) {
		die('Unable to connect to database ['.$db->connect_error.']');
	}

	if ($_GET['id']) {

		$actor_id = $_GET['id'];

		// Get actor information
		$actor = $db->query("
			SELECT *
			FROM Actor
			WHERE id={$actor_id}
		");

		// Movies and roles actor was in
		$movies = $db->query("
			SELECT M.id, M.title, MA.role
			FROM MovieActor MA, Movie M
			WHERE aid={$actor_id} AND MA.mid=M.id
		");

		$actor_html = "<table border=\"1\">";
		$movies_html = "<table border=\"1\">";

		// Add table headers
		$actor_html .= "
			<tr>
				<td><b>Name</b></td>
				<td><b>Sex</b></td>
				<td><b>Date of Birth</b></td>
				<td><b>Date of Death</b></td>
			</tr>
		";

		// Add table headers
		$movies_html .= "
			<tr>
				<td><b>Role</b></td>
				<td><b>Title</b></td>
			</tr>
		";

		// Create actor information html
		while ($row = $actor->fetch_array()) {
			$first = $row['first'];
			$last = $row['last'];
			$sex = $row['sex'];
			$dob = $row['dob'];
			$dod = $row['dod'] ? $row['dod'] : 'Still Alive';

			$actor_html .= "
				<tr>
					<td>{$first} {$last}</td>
					<td>{$sex}</td>
					<td>{$dob}</td>
					<td>{$dod}</td>
				</tr>
			";
		}

		// Create list of roles
		while ($row = $movies->fetch_array()) {
			$role = $row['role'];
			$movie_id = $row['id'];
			$title = $row['title'];

			$movies_html .= "
				<tr>
					<td>{$role}</td>
					<td><a href=\"movie_info.php?id={$movie_id}\">{$title}</td>
				</tr>
			";
		}

		$actor_html .= "</table>";
		$movies_html .= "</table>";
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link href="https://fonts.googleapis.com/css?family=Lato|Lobster" rel="stylesheet">
	<link rel="stylesheet" href="css/app.css">
</head>
<body>
	<div class="page">
		<div class="row">
			<div class="header">
				<h2>Actor Information</h2>	
			</div>	
		</div>
		<div class="row">
			<div class="sidebar">
				<ul class="nav">
					<p class="navtitle">Home</p>
					<li>
						<a href="index.php">Home</a>
					</li>
				</ul>
				<ul class="nav">
					<p class="navtitle">Add new content</p>
					<li>
						<a href="add_actor_director.php">Add Actor/Director</a>
					</li>
					<li>
						<a href="add_movie_info.php">Add Movie Information</a>
					</li>
					<li>
						<a href="add_movie_actor_relation.php">Add Movie/Actor Relation</a>
					</li>
					<li>
						<a href="add_movie_director_relation.php">Add Movie/Director Relation</a>
					</li>
				</ul>
				<ul class="nav">
					<p class="navtitle">Search</p>
					<li>
						<a href="search.php">Search Actor/Movie</a>
					</li>
				</ul>
			</div>
			<div class="content">
				<div class="actor_info">
					<h2>Actor Information</h2>
					<?= $actor_html ?>
				</div>
				<div class="movies_info">
					<h2>Actor's Movies and Role</h2>
					<?= $movies_html ?>
				</div>
			</div>		
		</div>
	</div>
</body>
</html>