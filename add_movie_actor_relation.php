<?php 
	// Connect to Database //
	$db = new mysqli('localhost', 'cs143', '', 'CS143');

	// Connect to db //
	if ($db->connect_errno > 0) {
		die('Unable to connect to database ['.$db->connect_error.']');
	}

	$actors = $db->query("
		SELECT id, first, last, dob
		FROM Actor
		ORDER BY first
	");

	$movies = $db->query("
		SELECT id, title, year
		FROM Movie
		ORDER BY title
	");

	$actorOptions = "";
	$movieOptions = "";

	// Create actor options
	while ($row = $actors->fetch_array()) {
		$id = $row["id"];
		$first = $row["first"];
		$last = $row["last"];
		$dob = $row["dob"];

		$actorOptions .= "<option value={$id}>{$first} {$last} ({$dob})</option>";
	}

	// Create movie options
	while ($row = $movies->fetch_array()) {
		$id = $row["id"];
		$title = $row["title"];
		$year = $row["year"];

		$movieOptions .= "<option value={$id}>{$title} ({$year})</option>";
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link href="https://fonts.googleapis.com/css?family=Lato|Lobster" rel="stylesheet">
	<link rel="stylesheet" href="css/app.css">
</head>
<body>
	<div class="page">
		<div class="row">
			<div class="header">
				<h2>Add Movie/Actor Relation</h2>
			</div>	
		</div>
		<div class="row">
			<div class="sidebar">
				<ul class="nav">
					<p class="navtitle">Home</p>
					<li>
						<a href="index.php">Home</a>
					</li>
				</ul>
				<ul class="nav">
					<p class="navtitle">Add new content</p>
					<li>
						<a href="add_actor_director.php">Add Actor/Director</a>
					</li>
					<li>
						<a href="add_movie_info.php">Add Movie Information</a>
					</li>
					<li>
						<a href="add_movie_actor_relation.php">Add Movie/Actor Relation</a>
					</li>
					<li>
						<a href="add_movie_director_relation.php">Add Movie/Director Relation</a>
					</li>
				</ul>
				<ul class="nav">
					<p class="navtitle">Search</p>
					<li>
						<a href="search.php">Search Actor/Movie</a>
					</li>
				</ul>
			</div>
			<div class="content">
				<form method="GET" action="#">
					<div class="form-group">
						<label for="movieid">Movie</label>
						<select name="movieid" id="">
							<?= $movieOptions ?>
						</select>
					</div>
					<div class="form-group">
						<label for="actorid">Actor</label>
						<select name="actorid" id="">
							<?= $actorOptions ?>
						</select>
					</div>
					<div class="form-group">
						<label for="role">Role</label>
						<input name="role" type="text">
					</div>
					<button type="submit">Add</button>
				</form>
			</div>		
		</div>
	</div>

	<?php
		if ($_GET) {

			$actor_id = $_GET['actorid'];
			$movie_id = $_GET['movieid'];
			$role = $_GET['role'];

			// Validate inputs
			if (!$role)
				echo "should have a role";

			// Inputs are valid
			else {
				$query = "INSERT INTO MovieActor (mid, aid, role) VALUES($movie_id, $actor_id, '$role')";

				if ($db->query($query))
					echo "Added successfully";
				else
					echo "Error: " . $query . "<br/>" . $db->error;
			}
		}

		$db->close();
	?>

</body>
</html>