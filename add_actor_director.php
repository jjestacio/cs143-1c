<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link href="https://fonts.googleapis.com/css?family=Lato|Lobster" rel="stylesheet">
	<link rel="stylesheet" href="css/app.css">
</head>
<body>
	<div class="page">
		<div class="row">
			<div class="header">
				Add Actor/Director
			</div>	
		</div>
		<div class="row">
			<div class="sidebar">
				<ul class="nav">
					<p class="navtitle">Home</p>
					<li>
						<a href="index.php">Home</a>
					</li>
				</ul>
				<ul class="nav">
					<p class="navtitle">Add new content</p>
					<li>
						<a href="add_actor_director.php">Add Actor/Director</a>
					</li>
					<li>
						<a href="add_movie_info.php">Add Movie Information</a>
					</li>
					<li>
						<a href="add_movie_actor_relation.php">Add Movie/Actor Relation</a>
					</li>
					<li>
						<a href="add_movie_director_relation.php">Add Movie/Director Relation</a>
					</li>
				</ul>
				<ul class="nav">
					<p class="navtitle">Search</p>
					<li>
						<a href="search.php">Search Actor/Movie</a>
					</li>
				</ul>
			</div>
			<div class="content">
				<form method="GET" action="#">
					<input type="radio" name="identity" value="Actor" checked> Actor
					<input type="radio" name="identity" value="Director"> Director
					<div class="form-group">
						<label for="fname">
							First Name
						</label>
						<input type="text" class="text-input" placeholder="Text input" name="fname">
					</div>
					<div class="form-group">
						<label for="lname">
							Last Name
						</label>
						<input type="text" class="text-input" placeholder="Text input" name="lname">
					</div>
					<input type="radio" name="sex" value="male" checked> Male
					<input type="radio" name="sex" value="female"> Female
					<div class="form-group">
						<label for="dateb">
							Date of Birth
						</label>
						<input type="text" class="text-input" placeholder="Text input" name="dateb">
					</div>
					<div class="form-group">
						<label for="dated">
							Date of Die
						</label>
						<input type="text" class="text-input" placeholder="Text input" name="dated">
					</div>
					<button type="submit">Add</button>
				</form>
			</div>		
		</div>
	</div>

	<?php 

		// Connect to Database //
		$db = new mysqli('localhost', 'cs143', '', 'CS143');

		// Connect to db //
		if ($db->connect_errno > 0) {
			die('Unable to connect to database ['.$db->connect_error.']');
		}

		// Process input //
		if ($_GET) {

			$identity = $_GET['identity'];
			$fname = $_GET['fname'];
			$lname = $_GET['lname'];
			$sex = $_GET['sex'];
			$dateb = $_GET['dateb'];
			$dated = $_GET['dated'];

			$dateb_split = explode('-', $dateb);
			$dated_split = explode('-', $dated);

			// Validate inputs //
			if (!$fname)
				echo "First name is empty. <br/>";
			else if (!$lname)
				echo "Last name is empty. <br/>";
			else if (!checkdate($dateb_split[1], $dateb_split[2], $dateb_split[0]))
				echo "Date of birth is not valid. <br/>";
			else if ($dated != '' && !checkdate($dated_split[1], $dated_split[2], $dated_split[0]))
				echo "Date of death is not valid. <br/>";
			
			// Input is valid //
			else {

				// Get current max person id
				$maxPersonID = $db->query(
					"SELECT * 
					 FROM MaxPersonID"
				)->fetch_assoc()['id'];

				$newMaxPersonId = $maxPersonID + 1;

				// Choose appropriate query
				if ($identity == 'Actor') {
					$query = $dated == '' // Set date of death to NULL if no value entered
						? "INSERT INTO Actor (id, last, first, sex, dob, dod) VALUES($newMaxPersonId, '$lname', '$fname', '$sex', '$dateb', NULL)"
						: "INSERT INTO Actor (id, last, first, sex, dob, dod) VALUES($newMaxPersonId, '$lname', '$fname', '$sex', '$dateb', '$dated')";
				} else {
					$query = $dated == '' // Set date of death to NULL if no value entered
						? "INSERT INTO Director (id, last, first, dob, dod) VALUES($newMaxPersonId, '$lname', '$fname', '$dateb', NULL)"
						: "INSERT INTO Director (id, last, first, dob, dod) VALUES($newMaxPersonId, '$lname', '$fname', '$dateb', '$dated')";
				}
				
				if ($db->query($query)) {

					// Update max person id
					$db->query("
						UPDATE MaxPersonID
						SET id=$newMaxPersonId
						WHERE id=$maxPersonID
					");

					echo "Added successfully";
				} else {
					echo "Error: " . $query . "<br/>" . $db->error;
				}
			}
		}

		$db->close();
	?>
</body>
</html>

