-- Movie must be made after 1800 and before 2020
-- and have ratings G, PG, PG-13, R, NC-17, or NULL 
CREATE TABLE Movie (
	id INT NOT NULL,
	title VARCHAR(100) NOT NULL,
	year INT,
	rating VARCHAR(10),
	company VARCHAR(50),
	PRIMARY KEY(id),
	CHECK(rating = 'G' OR rating = 'PG' or rating = 'PG-13'
		or rating = 'R' or rating = 'NC-17' or rating IS NULL),
	CHECK(1800 < year AND year < 2020)
) ENGINE=INNODB;

-- Gender of actor must be male or female
CREATE TABLE Actor (
	id INT NOT NULL,
	last VARCHAR(20),
	first VARCHAR(20),
	sex VARCHAR(6),
	dob DATE NOT NULL,
	dod DATE,
	PRIMARY KEY(id),
	CHECK(sex='Male' or sex='Female')
) ENGINE=INNODB;

CREATE TABLE Director (
	id INT NOT NULL,
	last VARCHAR(20),
	first VARCHAR(20),
	dob DATE NOT NULL,
	dod DATE,
	PRIMARY KEY(id)
) ENGINE=INNODB;

-- the movie must exist in the Movie table
CREATE TABLE MovieGenre (
	mid INT NOT NULL,
	genre VARCHAR(20),
	FOREIGN KEY(mid) references Movie(id)
) ENGINE=INNODB;

-- the movie must exist in the Movie table
-- the director must exist in the Director table
CREATE TABLE MovieDirector (
	mid INT NOT NULL,
	did INT NOT NULL,
	FOREIGN KEY(mid) references Movie(id), 
	FOREIGN KEY(did) references Director(id)
) ENGINE=INNODB;

-- the movie must exist in the Movie table
-- the actor must exist in the Actor table
CREATE TABLE MovieActor (
	mid INT,
	aid INT,
	role VARCHAR(50),
	FOREIGN KEY(mid) references Movie(id), 
	FOREIGN KEY(aid) references Actor(id) 
) ENGINE=INNODB;

-- the movie must exist in the Movie table
CREATE TABLE Review (
	name VARCHAR(20),
	time TIMESTAMP,
	mid INT NOT NULL,
	rating INT NOT NULL,
	comment VARCHAR(500),
	FOREIGN KEY(mid) references Movie(id) 
) ENGINE=INNODB;

CREATE TABLE MaxPersonID (
	id INT NOT NULL
) ENGINE=INNODB;

CREATE TABLE MaxMovieID (
	id INT NOT NULL
) ENGINE=INNODB;