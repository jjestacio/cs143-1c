--------------------------
Project 1C

Johnathan Estacio
Sean Kim

--------------------------
Criteria Met

Search Actors/Movies
- Search using LIKE to more precisely get results
- Displays both actors and movies
	Links to corresponding pages

Add Actors/Directors/Movies
- Add actors, directors, movies
- Input validation
- Can select multiple genres
- All added data will show in the searches

Add Actor/Movie Relations
- Add an actor from a list of registered actors with a user defined role in a movie from a list of registered movies
- Relation shows in actor and movie info

Add Director/Movie Relations
- Add a director from a list of registered directors with a movie from a list of registered movies
- Relation shows in movie info

Add Reviews
- Add reviews for movies including correct timestamps

View Actor Info
- Linked to through the search interface
- See all actor info
- See roles of actors in movies
	- Links to corresponding movies

View Movie Info
- See all movie info
- Roles with actors
	- Links to corresponding actor pages
- See multiple genres
- See user added reviews
- See directors

------------------------------
Teamwork

We each implemented different files. Since the HTML was the same, we used a skeleton for each file and revised it as we went along.

We can improve on our collaboration by being clear with our code using comments and practice common coding styles to not confuse one another.


